## Speed run

A speedrun is a play-through (or a recording thereof, namely run) of a video game performed with the intention of completing it as fast as possible within one's own ability. [Wikipedia](https://en.wikipedia.org/wiki/Speedrun)

---

## What's this project?

This is just a project to test the [speedrun.com](https://www.speedrun.com) [API](https://github.com/speedruncomorg/api) with Android.
