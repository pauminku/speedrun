package com.minkusoft.speedrun

import android.support.test.runner.AndroidJUnit4
import com.minkusoft.speedrun.data_provider.ApiClient
import com.minkusoft.speedrun.data_provider.dto.GameDtoList
import com.minkusoft.speedrun.data_provider.dto.RunDto
import com.minkusoft.speedrun.data_provider.dto.UserDtoResult
import io.reactivex.observers.TestObserver
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ApiCallsTests {

    /**
     * makes an API call to get games list and check it returns an observable that emits one response
     * with correct data and then ends
     * NOTE: it needs an Internet connection to work
     */
    @Test
    fun getGamesListTest() {
        val testObserver = TestObserver<GameDtoList>()
        ApiClient.getGamesList().subscribe(testObserver)
        testObserver.awaitTerminalEvent() //wait the API call to finish
        //wait api to finish as a single
        waitAndTestIsSingle(testObserver)

        //check that data is correct
        val result = testObserver.values()[0]
        assertNotNull("result should not be null", result)
        assertNotEquals("result data should not be empty", 0, result.data.size)
        result.data.forEach {
            assertNotEquals("id should not be empty", 0, it.id)
        }
    }

    /**
     * makes an API call to get the first run of an arbitrary game (actually the first one) and check it returns an
     * observable that emits one response with correct data and then ends
     * NOTE: it needs an Internet connection to work
     */
    @Test
    fun getGameRunsTest() {
        //ask first for a valid id to work with
        val auxObserver = TestObserver<GameDtoList>()
        ApiClient.getGamesList().subscribe(auxObserver)
        //wait api to finish as a single
        waitAndTestIsSingle(auxObserver)

        //makes the second api call, the one that we want to test here
        val firstId = auxObserver.values()[0].data[0].id
        val testObserver = TestObserver<RunDto>()
        ApiClient.getGameFirstRun(firstId).subscribe(testObserver)
        //wait api to finish as a single
        waitAndTestIsSingle(testObserver)

        //check that data is correct
        val result = testObserver.values()[0]
        assertNotNull("result should not be null", result)
        assertFalse("primary time should be greater than 0", result.times.primary_t <= 0)
    }

    /**
     * makes an API call to get user details and check it returns an observable that emits one response
     * with correct data and then ends
     * NOTE: it needs an Internet connection to work
     */
    @Test
    fun getUserDetail() {
        val testId = "wzx7q875"
        val testObserver = TestObserver<UserDtoResult>()
        ApiClient.getUserDetail(testId).subscribe(testObserver)
        //wait api to finish as a single
        waitAndTestIsSingle(testObserver)

        //check that data is correct
        val result = testObserver.values()[0]
        assertNotNull("result should not be null", result)
        assertNotEquals("name should not be empty", 0, result.data.names.international.length)
    }

    private fun <T> waitAndTestIsSingle(testObserver: TestObserver<T>) {
        //wait the API call to finish
        testObserver.awaitTerminalEvent()
        //check that observable emmits one item and then ends
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
    }
}
