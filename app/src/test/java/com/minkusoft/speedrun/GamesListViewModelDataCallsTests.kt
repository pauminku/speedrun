package com.minkusoft.speedrun

import com.minkusoft.speedrun.data_provider.GameListDataProviderInterface
import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.view_model.list.GamesListViewModel
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Test

class GamesListViewModelDataCallsTests {
    /**
     * Data provider to inject to ViewModel to be able to test it without API dependency
     */
    class MockDataProvider : GameListDataProviderInterface {
        companion object {
            const val TOTAL = 20
        }

        override fun getGamesFirstPage(): Single<List<Game>> {
            return Single.just((1..TOTAL).map { Game("$it", "fake Game number $it", null) })
        }
    }

    /**
     * Data provider to test errors
     */
    class MockErrorDataProvider : GameListDataProviderInterface {
        override fun getGamesFirstPage(): Single<List<Game>> {
            return Single.error(RuntimeException("fake error"))
        }
    }


    /**
     * Test if DataProvider (API) is called when view is subscribed to viewmodel data
     */
    @Test
    fun testFirstApiCall() {
        val speedrunsListViewModel = GamesListViewModel()
        speedrunsListViewModel.provider = MockDataProvider()
        val testObserver = TestObserver<List<Game>>()
        speedrunsListViewModel.getGamesListObservable().subscribe(testObserver)
        testObserver.assertValueCount(1)//data received
        assertEquals(
            "games should be the same as emmited by MockDataProvider",
            MockDataProvider.TOTAL,
            testObserver.values()[0].size
        )
    }

    /**
     * Test if DataProvider (API) is called when view model is asked to refresh data
     */
    @Test
    fun testRefreshCall() {
        val speedrunsListViewModel = GamesListViewModel()
        speedrunsListViewModel.provider = MockDataProvider()
        val testObserver = TestObserver<List<Game>>()
        speedrunsListViewModel.getGamesListObservable().subscribe(testObserver)
        speedrunsListViewModel.refreshList()
        testObserver.assertValueCount(2)//first call and refresh call
    }

    /**
     * Test if an error on DataProvider (API) emits an error to view
     */
    @Test
    fun errorEmitted() {
        val speedrunsListViewModel = GamesListViewModel()
        speedrunsListViewModel.provider = MockErrorDataProvider()
        val testObserver = TestObserver<Throwable>()
        speedrunsListViewModel.errorsObservable.subscribe(testObserver)
        speedrunsListViewModel.getGamesListObservable().subscribe()
        testObserver.assertValueCount(1)//error received
    }

    /**
     * Test if an error on DataProvider (API) don't terminate the data observable,
     * allowing, for instance, to manually refresh
     */
    @Test
    fun errorDontTerminateDataObservable() {
        val speedrunsListViewModel = GamesListViewModel()
        val dataObserver = TestObserver<List<Game>>()
        speedrunsListViewModel.provider = MockErrorDataProvider()
        val testObserver = TestObserver<Throwable>()
        speedrunsListViewModel.errorsObservable.subscribe(testObserver)
        speedrunsListViewModel.getGamesListObservable().subscribe(dataObserver)
        dataObserver.assertNotTerminated()//error on api don't terminate the data observable
    }

}