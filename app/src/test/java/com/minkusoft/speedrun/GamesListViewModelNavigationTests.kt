package com.minkusoft.speedrun

import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.view_model.SpeedrunViewModel
import com.minkusoft.speedrun.view_model.list.GamesListViewModel
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertTrue
import org.junit.Test

class GamesListViewModelNavigationTests {

    /**
     * Test if the action to select a game fires the appropriate navigate event
     */
    @Test
    fun gameSelectedNavigation() {
        val speedrunsListViewModel = GamesListViewModel()
        val testObserver = TestObserver<GamesListViewModel.ListNavigate>()
        speedrunsListViewModel.navigateObservable.subscribe(testObserver)
        speedrunsListViewModel.gameSelected(Game("id", "fakeGame", null))
        testObserver.assertValueCount(1)
        assertTrue(testObserver.values()[0] is GamesListViewModel.ListNavigateToDetail)
        testObserver.assertNotComplete()
        speedrunsListViewModel.onCleared()
        testObserver.assertComplete()
    }

    /**
     * Test if onCleared disposes the observables
     */
    @Test
    fun onClearedDisposeSubscriptions() {
        val speedrunsListViewModel = GamesListViewModel()
        val testNavigateObserver = TestObserver<GamesListViewModel.ListNavigate>()
        val testLoadingObserver = TestObserver<Boolean>()
        val testErrorObserver = TestObserver<Throwable>()
        speedrunsListViewModel.navigateObservable.subscribe(testNavigateObserver)
        speedrunsListViewModel.updatingObservable.subscribe(testLoadingObserver)
        speedrunsListViewModel.errorsObservable.subscribe(testErrorObserver)
        val testObservers = arrayOf(testNavigateObserver, testLoadingObserver, testErrorObserver)
        //before onCleared the observables are not completed
        for (testObserver in testObservers) {
            testObserver.assertNotComplete()
        }
        speedrunsListViewModel.onCleared()
        //after onCleared the observables are completed
        for (testObserver in testObservers) {
            testObserver.assertComplete()
        }
        //an action does not fire events before onCleared
        speedrunsListViewModel.gameSelected(Game("id", "fakeGame", null))
        testNavigateObserver.assertValueCount(0)
    }


    /**
     * we use reflection to access protected methods, we could just change onCleared() visibility to public but is not to
     * change it just for tests, reflection is slow and ugly, but this is a test
     */
    private fun <T> SpeedrunViewModel<T>.onCleared() {
        val method = SpeedrunViewModel::class.java.getDeclaredMethod("onCleared")
        method.isAccessible = true
        method.invoke(this)
    }
}
