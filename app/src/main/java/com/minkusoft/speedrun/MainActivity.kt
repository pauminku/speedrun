package com.minkusoft.speedrun

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.facebook.drawee.backends.pipeline.Fresco
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        setContentView(R.layout.activity_main)

        navController = navHostFragment.findNavController()
        setupActionBarWithNavController(navController)
    }

    override fun onBackPressed() {
        if (!navController.popBackStack()) super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.popBackStack() || super.onNavigateUp()
    }
}
