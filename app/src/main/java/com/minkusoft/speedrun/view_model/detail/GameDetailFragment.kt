package com.minkusoft.speedrun.view_model.detail

import android.arch.lifecycle.ViewModelProviders
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.navigation.NavDirections
import com.minkusoft.speedrun.R
import com.minkusoft.speedrun.view_model.SpeedrunFragment
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.game_detail_fragment.*
import java.util.concurrent.TimeUnit

class GameDetailFragment : SpeedrunFragment<GameDetailViewModel.DetailNavigate, GameDetailViewModel>() {

    companion object {
        fun formatTime(seconds: Float): String {
            val millis = (seconds * 1000f).toLong()
            return String.format(
                "%02dh %02dm %02ds",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
            )
        }
    }
    override fun createViewModel(): GameDetailViewModel {
        return ViewModelProviders.of(this).get(GameDetailViewModel::class.java).apply {
            game = arguments?.let { GameDetailFragmentArgs.fromBundle(it).game }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //actions
        rfGamesList.setOnRefreshListener { viewModel?.refreshGame() }
        btVideo.setOnClickListener { viewModel?.videoClick() }
    }

    override fun layoutResource() = R.layout.game_detail_fragment

    override fun getNavDirection(navEvent: GameDetailViewModel.DetailNavigate): NavDirections? {
        if (navEvent is GameDetailViewModel.DetailNavigateToVideo) {
            val uri = Uri.parse(navEvent.url)
            val youtubeUri =
                if (navEvent.url.contains("youtube.com")) {
                    uri.getQueryParameter("v")?.let { Uri.parse("vnd.youtube:$it") }
                } else {
                    null
                }

            try {
                startActivity(Intent(Intent.ACTION_VIEW, youtubeUri ?: uri))
            } catch (ex: ActivityNotFoundException) {
                println("No youtube app installed or link not from youtube")
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, uri))
                } catch (ex: ActivityNotFoundException) {
                    showException(RuntimeException("There's no app to open this url: ${navEvent.url}"))
                }
            }
        }
        return null
    }

    override fun updatingVisibility(visible: Boolean) {
        rfGamesList.isRefreshing = visible
    }

    override fun subscribeToData(): Disposable? {
        viewModel?.game?.let {
            txtName.text = it.gameName
            imgLogo.setImageURI(it.logo)
        }
        return viewModel?.getGameObservable()?.subscribe(
            {
                txtTime.text = formatTime(it.time)
                txtPlayerName.text = it.playerName
                btVideo.isEnabled = !TextUtils.isEmpty(it.videoUrl)
            },
            { showException(it) })
    }

}
