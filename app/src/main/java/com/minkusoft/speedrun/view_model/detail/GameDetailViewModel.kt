package com.minkusoft.speedrun.view_model.detail

import com.minkusoft.speedrun.data_provider.DetailDataProvider
import com.minkusoft.speedrun.data_provider.DetailDataProviderInterface
import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.model.Run
import com.minkusoft.speedrun.view_model.SpeedrunViewModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

class GameDetailViewModel : SpeedrunViewModel<GameDetailViewModel.DetailNavigate>() {
    interface DetailNavigate
    data class DetailNavigateToVideo(val url: String) : DetailNavigate

    var game: Game? = null

    var provider: DetailDataProviderInterface = DetailDataProvider()
    private var gameSubject = BehaviorSubject.create<Run>()

    fun getGameObservable(): Observable<Run> {
        if (!gameSubject.hasValue()) {
            refreshGame()
        }
        return gameSubject.hide()
    }

    fun refreshGame() {
        if (game == null) {
            errorOccurred(RuntimeException("No game defined"))
        } else {
            compositeDisposable.add(
                provider.getRunDetail(game!!).configureLoadingSingle().subscribe(
                    {
                        if (!gameSubject.hasComplete()) {
                            //we are not disposing the api call, so we need to check if the subject is still alive
                            gameSubject.onNext(it)
                        }
                    },
                    { errorOccurred(it) }
                ))
        }
    }

    fun videoClick() {
        gameSubject.value?.videoUrl?.let { navigateSubject.onNext(DetailNavigateToVideo(it)) }
            ?: errorOccurred(RuntimeException("No video found for this run"))
    }
}
