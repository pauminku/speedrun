package com.minkusoft.speedrun.view_model

import android.arch.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * abstract ViewModel to generete the common observables for viewmodel lifecicle
 */
abstract class SpeedrunViewModel<T> : ViewModel() {

    //subject that emits actions that view has to catch to navigate
    protected val navigateSubject = PublishSubject.create<T>()
    val navigateObservable = navigateSubject.throttleFirst(
        500,
        TimeUnit.MILLISECONDS
    ).hide()!!//throttleFirst to avoid flags to check sequential actions, for example two quick consecutive taps

    //subject that emits Throwable that view has to catch to show exceptions
    private val errorsSubject = PublishSubject.create<Throwable>()
    val errorsObservable = errorsSubject.hide()!!

    //subject that emits booleans that view has to catch to show some activity progress to the user
    private val updatingSubject = BehaviorSubject.createDefault(false)
    val updatingObservable = updatingSubject.hide()!!

    protected val compositeDisposable = CompositeDisposable()
    override fun onCleared() {
        super.onCleared()
        //we terminate the subjects that we are not needing anymore
        navigateSubject.onComplete()
        updatingSubject.onComplete()
        errorsSubject.onComplete()
        compositeDisposable.clear()
    }

    /**
     * convenient way to inform interested subjects when an error occurs
     */
    protected fun errorOccurred(throwable: Throwable) {
        errorsSubject.onNext(throwable)
        updatingSubject.onNext(false)
    }

    /**
     * convenient way to configure observables that we want the view to show some activity indicator while in progress
     */
    protected fun <T> Single<T>.configureLoadingSingle(): Single<T> {
        return this.doOnSubscribe { updatingSubject.onNext(true) }
            .doFinally { updatingSubject.onNext(false) }
    }
}