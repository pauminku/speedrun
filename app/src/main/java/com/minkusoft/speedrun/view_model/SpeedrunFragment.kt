package com.minkusoft.speedrun.view_model

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * abstract Fragment to do common behavior of fragments using SpeedrunViewModel as ViewModel
 */
abstract class SpeedrunFragment<M, T : SpeedrunViewModel<M>> : Fragment() {

    private var compositeDisposable: CompositeDisposable? = null

    protected var viewModel: T? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutResource(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToViewModelEvents()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable?.dispose() //unsubscribe to avoid leaks
    }

    /**
     * subscribe the view to common viewmodel's observables to respond to viewmodel's events
     */
    private fun subscribeToViewModelEvents() {
        //subscription to navigation events
        val navDisposable = viewModel?.navigateObservable?.subscribe({ navEvent ->
            getNavDirection(navEvent)?.let { findNavController().navigate(it) }
        }, {
            showException(it)
        })
        //subscribe to updating events to show some progress view while loading whatever that's loading on viewmodel
        val updDisposable = viewModel?.updatingObservable?.subscribe({
            updatingVisibility(it)
        }, {
            showException(it)
        })
        //subscribe to error events
        val errorDisposable = viewModel?.errorsObservable?.subscribe({
            showException(it)
        }, {
            showException(it)
        })
        //subscribe to data
        val listDisposable = subscribeToData()
        //add all disposables to a single composite disposable to make it easy to dispose
        compositeDisposable?.dispose() //just in case
        compositeDisposable = CompositeDisposable()
        compositeDisposable!!.addAll(navDisposable, updDisposable, errorDisposable, listDisposable)
    }

    protected open fun showException(throwable: Throwable) {
        throwable.printStackTrace()
        //TODO show a custom message to user
        Snackbar.make(view!!, throwable.message.toString(), Snackbar.LENGTH_LONG).show()
    }

    protected abstract fun createViewModel(): T
    protected abstract fun layoutResource(): Int
    /**
     * NavDirection to go when a navigation event is emitted by viewmodel
     */
    protected abstract fun getNavDirection(navEvent: M): NavDirections?

    /**
     * to show some progress view while loading whatever that's loading on viewmodel
     */
    protected abstract fun updatingVisibility(visible: Boolean)

    /**
     * subscribe to show viewmodel data on view
     */
    protected abstract fun subscribeToData(): Disposable?

}