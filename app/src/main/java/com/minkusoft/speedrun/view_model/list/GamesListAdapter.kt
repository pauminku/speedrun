package com.minkusoft.speedrun.view_model.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.minkusoft.speedrun.R
import com.minkusoft.speedrun.model.Game
import kotlinx.android.synthetic.main.adapter_game.view.*

class GamesListAdapter : RecyclerView.Adapter<GamesListAdapter.GameHolder>() {

    inner class GameHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(game: Game) {
            itemView.txtName.text = game.gameName
            itemView.setOnClickListener { gameClicked?.invoke(game) }
            itemView.imgLogo.setImageURI(game.logo)
        }
    }

    var items: List<Game>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var gameClicked: ((Game) -> Unit)? = null

    private var layoutInflater: LayoutInflater? = null
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): GameHolder {
        layoutInflater = layoutInflater ?: LayoutInflater.from(parent.context)
        val view = layoutInflater!!.inflate(R.layout.adapter_game, parent, false)
        return GameHolder(view)
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(viewHolder: GameHolder, position: Int) {
        items?.get(position)?.let { viewHolder.bind(it) }
    }
}