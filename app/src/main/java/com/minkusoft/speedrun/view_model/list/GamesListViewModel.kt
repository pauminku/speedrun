package com.minkusoft.speedrun.view_model.list

import com.minkusoft.speedrun.data_provider.GameListDataProvider
import com.minkusoft.speedrun.data_provider.GameListDataProviderInterface
import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.view_model.SpeedrunViewModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

class GamesListViewModel : SpeedrunViewModel<GamesListViewModel.ListNavigate>() {
    interface ListNavigate
    data class ListNavigateToDetail(val game: Game) : ListNavigate

    var provider: GameListDataProviderInterface = GameListDataProvider()
    private var gamesSubject = BehaviorSubject.create<List<Game>>()

    fun getGamesListObservable(): Observable<List<Game>> {
        if (!gamesSubject.hasValue()) {
            refreshList()
        }
        return gamesSubject.hide()
    }

    fun refreshList() {
        compositeDisposable.add(
            provider.getGamesFirstPage().configureLoadingSingle().subscribe(
                {
                    if (!gamesSubject.hasComplete()) {
                        //we are not disposing the api call, so we need to check if the subject is still alive
                        gamesSubject.onNext(it)
                    }
                },
                { errorOccurred(it) }
            )
        )
    }

    fun gameSelected(game: Game) {
        navigateSubject.onNext(ListNavigateToDetail(game))
    }

    override fun onCleared() {
        super.onCleared()
        gamesSubject.onComplete()
    }
}
