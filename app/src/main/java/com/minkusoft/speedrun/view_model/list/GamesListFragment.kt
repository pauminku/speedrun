package com.minkusoft.speedrun.view_model.list

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import androidx.navigation.NavDirections
import com.minkusoft.speedrun.R
import com.minkusoft.speedrun.view_model.SpeedrunFragment
import kotlinx.android.synthetic.main.games_list_fragment.*

class GamesListFragment : SpeedrunFragment<GamesListViewModel.ListNavigate, GamesListViewModel>() {

    private val adapter = GamesListAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //configure list
        lsGamesList.layoutManager = LinearLayoutManager(activity)
        lsGamesList.adapter = adapter

        //actions
        adapter.gameClicked = { viewModel?.gameSelected(it) }
        rfGamesList.setOnRefreshListener { viewModel?.refreshList() } //refresh data
    }

    override fun createViewModel() = ViewModelProviders.of(this).get(GamesListViewModel::class.java)

    override fun layoutResource() = R.layout.games_list_fragment

    override fun updatingVisibility(visible: Boolean) {
        rfGamesList.isRefreshing = visible
        if (visible || adapter.itemCount > 0) {
            txtNoData.visibility = View.GONE
        }
    }

    override fun subscribeToData() = viewModel?.getGamesListObservable()?.subscribe(
        { adapter.items = it },
        { showException(it) }
    )


    override fun getNavDirection(navEvent: GamesListViewModel.ListNavigate): NavDirections? {
        return if (navEvent is GamesListViewModel.ListNavigateToDetail) {
            GamesListFragmentDirections.actionListToDetail(navEvent.game)
        } else {
            null
        }
    }

    override fun showException(throwable: Throwable) {
        super.showException(throwable)
        if (adapter.itemCount == 0) {
            txtNoData.visibility = View.VISIBLE
        }
    }

}
