package com.minkusoft.speedrun.data_provider

import com.minkusoft.speedrun.data_provider.dto.createGameModelList
import com.minkusoft.speedrun.data_provider.dto.createRunModel
import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.model.Run
import io.reactivex.Single

interface GameListDataProviderInterface {
    fun getGamesFirstPage(): Single<List<Game>>
}

class GameListDataProvider : GameListDataProviderInterface {
    override fun getGamesFirstPage(): Single<List<Game>> {
        return ApiClient.getGamesList().map { it.createGameModelList() }
    }
}


interface DetailDataProviderInterface {
    fun getRunDetail(game: Game): Single<Run>
}

class DetailDataProvider : DetailDataProviderInterface {
    override fun getRunDetail(game: Game): Single<Run> {
        return ApiClient.getGameFirstRun(game.id).map { it.createRunModel() }
            //flat map to check if we have the player name or have to ask it to API
            .flatMap { run ->
                if (run.playerName?.isNotEmpty() == true || run.playerId == null) {
                    Single.just(run)
                } else {
                    ApiClient.getUserDetail(run.playerId).map { run.apply { playerName = it.data.names.international } }
                }
            }
    }
}