package com.minkusoft.speedrun.data_provider

import com.minkusoft.speedrun.data_provider.dto.GameDtoList
import com.minkusoft.speedrun.data_provider.dto.RunDtoList
import com.minkusoft.speedrun.data_provider.dto.UserDtoResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SpeedrunService {
    @GET("/api/v1/games")
    fun getGamesList(): Observable<GameDtoList>

    @GET("/api/v1/runs")
    fun getRuns(@Query("game") gameId: String, @Query("max") max: Int = 20): Observable<RunDtoList>

    @GET("/api/v1/users/{id}")
    fun getUserDetail(@Path("id") id: String): Observable<UserDtoResult>
}