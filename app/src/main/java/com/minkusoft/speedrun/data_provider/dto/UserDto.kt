package com.minkusoft.speedrun.data_provider.dto

data class UserDto(val id: String, val names: Name)
data class UserDtoResult(val data: UserDto)