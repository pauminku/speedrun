package com.minkusoft.speedrun.data_provider

import com.minkusoft.speedrun.BuildConfig
import com.minkusoft.speedrun.data_provider.dto.GameDtoList
import com.minkusoft.speedrun.data_provider.dto.RunDto
import com.minkusoft.speedrun.data_provider.dto.RunDtoList
import com.minkusoft.speedrun.data_provider.dto.UserDtoResult
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * This object has the responsibility to configure the API  service and make the API calls
 */
object ApiClient {

    private const val BASE_URL = "https://www.speedrun.com"
    private const val CONNECTION_TIMEOUT = 10L

    private val service: SpeedrunService

    init {
        val clientBuilder = initClientBuilder()
        service = createRetrofit(clientBuilder).create(SpeedrunService::class.java)
    }

    private fun initClientBuilder(): OkHttpClient.Builder {
        val clientBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            //logging only on debug
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(interceptor)
        }
        //it seems sometime the Speedrun API is very slow, we need a long timout to avoid often TimeoutException's
        clientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        clientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        clientBuilder.protocols(listOf(Protocol.HTTP_1_1))
        return clientBuilder
    }

    private fun createRetrofit(clientBuilder: OkHttpClient.Builder): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//to work with RXJava instead of callbacks
            .addConverterFactory(GsonConverterFactory.create())//to deserialize and serialize json's
            .client(clientBuilder.build())
            .build()
    }

    fun getGamesList(): Single<GameDtoList> = configureObservable(service.getGamesList()).firstOrError()

    fun getGameRuns(gameId: String): Single<RunDtoList> = configureObservable(service.getRuns(gameId)).firstOrError()

    fun getGameFirstRun(gameId: String): Single<RunDto> = configureObservable(
        service.getRuns(
            gameId,
            1
        )
    ).map { if (it.data.isEmpty()) throw RuntimeException("No runs for this game") else it.data[0] }.firstOrError()

    fun getUserDetail(id: String): Single<UserDtoResult> = configureObservable(service.getUserDetail(id)).firstOrError()

    private fun <T> configureObservable(baseObservable: Observable<T>) = baseObservable
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .unsubscribeOn(Schedulers.io())

}