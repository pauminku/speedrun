package com.minkusoft.speedrun.data_provider.dto

data class Link(val uri: String)
data class Video(val links: ArrayList<Link>?)
data class Times(val primary_t: Float)
data class Player(val id: String?, val name: String?)
data class RunDto(val id: String, val videos: Video?, val times: Times, val players: ArrayList<Player>)
data class RunDtoList(val data: ArrayList<RunDto>)