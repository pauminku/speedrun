package com.minkusoft.speedrun.data_provider.dto

import com.google.gson.annotations.SerializedName

data class Name(val international: String)
data class Asset(val uri: String)
data class Assets(
    val logo: Asset?,
    @SerializedName("cover-large")
    val coverLarge: Asset?
)

data class GameDto(val id: String, val names: Name, val assets: Assets?)
data class GameDtoList(val data: ArrayList<GameDto>)