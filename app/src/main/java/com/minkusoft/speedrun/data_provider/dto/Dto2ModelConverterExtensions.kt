package com.minkusoft.speedrun.data_provider.dto

import com.minkusoft.speedrun.model.Game
import com.minkusoft.speedrun.model.Run


fun GameDto.createGameModel(): Game =
    Game(this.id, this.names.international, this.assets?.coverLarge?.uri ?: this.assets?.logo?.uri)

fun GameDtoList.createGameModelList(): List<Game> {
    return this.data.map { it.createGameModel() }
}

fun RunDto.createRunModel(): Run =
    Run(
        times.primary_t,
        videos?.links?.let { if (it.isEmpty()) null else it[0].uri },
        if (players.isEmpty()) null else players[0].id,
        if (players.isEmpty()) null else players[0].name
    )